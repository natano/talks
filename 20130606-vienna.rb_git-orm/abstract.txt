Implementing git-orm

Git-orm is a object-relational mapper on top of the Git scm implemented in
the Python programming language. It features a concise, django-esque model
syntax and an expressive query language.
This talk showcases the previously mentioned features and provides insight into
the implementation of git-orm and the syntactic sugar that makes the library
what it is.
