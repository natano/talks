\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{xcolor}

\pdsetup{palette=GoldenGate}
\lstset{
    language=Python,
    commentstyle=\color{blue},
    keywordstyle=\color{cyan},
    stringstyle=\color{magenta},
    escapeinside={\%*}{*)},
}

\lstnewenvironment{pycode}[1][]{
    \vspace{\stretch{1}}
    \lstset{language=Python,caption={Python},#1}
}{
    \vspace{\stretch{1}}
}

\lstnewenvironment{rbcode}[1][]{
    \vspace{\stretch{1}}
    \lstset{language=Ruby,caption={Ruby},#1}
}{
    \vspace{\stretch{1}}
}


\title{Implementing git-orm\\\small{\url{https://github.com/natano/python-git-orm/}}}
\author{Martin Natano}
\date{June 06, 2013}

\begin{document}

    \maketitle

    \begin{slide}{whoami}
        \vspace{\stretch{1}}
        \emph{Martin Natano}
        \begin{itemize}
            \item Web Developer @RadarServices
            \item previously: Medical University of Vienna, Mjam
        \end{itemize}
        \vspace{\stretch{1}}
    \end{slide}

    \begin{note}{}
        \paragraph{}
        Hey everyone!
        \paragraph{}
        My name is Martin Natano. I'm currently working at RadarServices and
        previously at the Medical University of Vienna and Mjam.
        \paragraph{}
        I'd like to present git-orm today.
    \end{note}

    \begin{slide}{Description}
        \vspace{\stretch{1}}
        \begin{itemize}
            \item django-esque model interface for storing objects in a git repository
            \item ISC licensed
            \item written in python
        \end{itemize}
        \vspace{\stretch{1}}
    \end{slide}

    \begin{note}{Greeting}
        \paragraph{}
        git-orm is a django-esque model interface for storing object in a git
        repository. It's written in the python programming language and freely
        available under the ISC license.
    \end{note}

    \section{Models}

    \begin{slide}[method=direct]{Models}
        \begin{pycode}[gobble=12]
            from git_orm import models

            class User(models.Model):
                email = models.TextField(primary_key=True)
                name = models.TextField(null=True)

            class Article(models.Model):
                author = models.ForeignKey(User)
                summary = models.TextField()
                content = models.TextField()
        \end{pycode}
    \end{slide}

    \begin{note}{What does it look like?}
        \paragraph{}
        A model definition looks like this.
        \paragraph{}
        Here we define a user and an article model. The user model has an
        explicit primary key.
        \paragraph{}
        Every model that doesn't define a primary key (like our Article model)
        will have one added automatically, namly a random UUID.
    \end{note}

    \begin{slide}[method=direct]{Creating an object}
        \begin{pycode}[gobble=12]
            hansel = User()
            hansel.email = 'hansel@example.com'
            hansel.name = 'Hans'
            hansel.save()

            # or

            gretel = User.create(email='grete@example.com',
                                 name='Grete')
        \end{pycode}
    \end{slide}

    \begin{note}{Creating an object}
        \paragraph{}
        Creating an object is easy. Just create an instance of the model class,
        assign some attributes and save it.
        \paragraph{}
        Alternatively we could do it in one call with the "create" method.
        \paragraph{}
        Every change creates a commit in the git repository. The path of an
        object in the repository consists of the model name and the value of
        the primary key field.
    \end{note}

    \begin{slide}[method=direct]{Timestamps}
        \begin{pycode}[gobble=12]
            >>> gretel.created_at
            datetime.datetime(2013, 6, 6, 12, 27, 35, 276071)
            >>> gretel.updated_at
            datetime.datetime(2013, 6, 6, 10, 08, 33, 3)
        \end{pycode}
    \end{slide}

    \begin{note}{}
        \paragraph{}
        Every objects has two timestamps: "created\_at" and "updated\_at". They
        are added automatically.
    \end{note}

    \section{Querysets}

    \begin{slide}[method=direct]{Expressive Syntax}
        \begin{pycode}[gobble=12]
            User.objects.get(email='user@example.org')
            User.objects.exists()
            Article.objects.all()
            Article.objects.count()
            Article.objects.filter(author_email='user@example.org')
            Article.objects.exclude(summary__contains='vienna.rb')
        \end{pycode}
    \end{slide}

    \begin{note}{Querying}
        \paragraph{}
        Querying is easy too. There are the methods "all", "filter", "exclude",
        "order\_by", "exists", "get" and "count".
    \end{note}

    \begin{slide}[method=direct]{Chaining}
        \begin{pycode}[gobble=12]
            (Article.objects
                .exclude(author_email='user@example.org')
                .filter(summary__icontains='vienna.rb')
                .count())
            User.objects.order_by('email')[:10]
        \end{pycode}
    \end{slide}

    \begin{note}{Chaining}
        \paragraph{}
        Most of them are chainable. What you see on the screen are perfectly
        valid statements.
        \paragraph{}
        Btw, slicing is also possible. (and chainable too)
    \end{note}

    \begin{slide}[method=direct]{Lazy Evaluation}
        \begin{pycode}[gobble=12]
            articles = Article.objects.all()
            articles = articles.filter(published_at__lt=now)
            articles.count()
        \end{pycode}
    \end{slide}

    \begin{note}{Lazyness}
        \paragraph{}
        All chainable methods are lazyly evaluated. So in our example the the
        first line produces a queryset that containes the conditions for the
        query we finally want to produce. The second line adds a condition to
        the query and the last line executes it.
    \end{note}

    \section{Q Objects}

    \begin{slide}[method=direct]{Q Objects (Advanced Querying)}
        \begin{pycode}[gobble=12]
            from git_orm.models import Q

            User.objects.filter(
                Q(email__endswith='@example.org')
                |Q(name__icontains='grete')
            )
        \end{pycode}
    \end{slide}

    \begin{note}{Q Objects}
        \paragraph{}
        Q objects are the centerpiece of queriesets. They represent a condition
        of a query. Direct use of them allows for complex queries that would not
        be possible with just using the "filter" and "exclude" methods.
    \end{note}

    \begin{slide}[method=direct]{Q Objects (connect 'em)}
        \begin{pycode}[gobble=12]
            from git_orm.models import Q

            ~Q(name='Hexe') & (
                Q(email='grete@example.com') | Q(name='Hansel'))
        \end{pycode}
    \end{slide}

    \begin{note}{Q Objects}
        \paragraph{}
        Possible operations are "and", "or", "not" and grouping with
        parentheses. Operator precedence is that of the basic operations in
        python. Just use parentheses when in doubt.
        \paragraph{}
        git-orm includes some basic query optimizations for minimizing query
        execution time.
    \end{note}

    \section{Transactions}

    \begin{slide}[method=direct]{Automatic Transaction Management}
        \begin{pycode}[gobble=12]
            from git_orm import transaction

            with transaction.wrap():
                %*\ldots*)
        \end{pycode}
    \end{slide}

    \begin{note}{Transactions (as a context manager)}
        \paragraph{}
        Transactions can help with grouping a collection of changes into one
        commit in the repository, so all of that changes are commited at once
        or none of them is.
    \end{note}

    \begin{slide}[method=direct]{Automatic Transaction Management}
        \begin{pycode}[gobble=12]
            @transaction.wrap()
            def persist():
                %*\ldots*)
        \end{pycode}
    \end{slide}

    \begin{note}{Transactions (as a context manager)}
        \paragraph{}
        When using transaction.wrap as a decorator you can wrap a whole
        function into a transaction
    \end{note}

    \begin{slide}[method=direct]{Manual Transaction Management}
        \begin{pycode}[gobble=12]
            from git_orm import transaction

            transaction.begin()
            %*\ldots*)
            transaction.commit()

            transaction.begin()
            %*\ldots*)
            transaction.rollback()
        \end{pycode}
    \end{slide}

    \begin{note}{Transactions (manually)}
        \paragraph{}
        If the automatic approach is not flexible enough you can fall back to
        the manual transaction handling primitives. When not using transactions
        at all, every update will be performed in a separate git commit.
    \end{note}


    \section{Syntactic Sugar}

    \begin{note}{}
        \paragraph{}
        For a nice API I implemented some syntactic sugar. As this is a ruby
        meetup I will show the ruby equivalent for every pattern.
    \end{note}

    \section{Context Managers}

    \begin{slide}[method=direct]{Context Managers}
        \begin{pycode}[gobble=12]
            with transaction.wrap() as trans:
                %*\ldots*)
        \end{pycode}
        \begin{rbcode}[gobble=12]
            Transaction.wrap do |trans|
                %*\ldots*)
            end
        \end{rbcode}
    \end{slide}

    \begin{note}{}
        \paragraph{}
        git-orm uses context managers for transactions. Python has a dedicated
        syntax for context managers. In ruby we have the more generic syntax of
        passing a block to a function.
    \end{note}

    \begin{slide}[method=direct]{Context Managers}
        \begin{pycode}[gobble=12]
            class wrap:
                %*\ldots*)
                def __enter__(self):
                    begin()
                    return _transaction

                def __exit__(self, type, value, traceback):
                    if not type and _transaction.has_changes:
                        commit(self.message)
                    else:
                        rollback()
                %*\ldots*)
        \end{pycode}
    \end{slide}

    \begin{note}{}
        \paragraph{}
        Before the code in the with block is executed, the \_\_enter\_\_ method
        is called. After the block finished (or raised an exception) the
        \_\_exit\_\_ method is called.
    \end{note}

    \section{Decorators}

    \begin{slide}[method=direct]{Decorators}
        \begin{pycode}[gobble=12]
            @transaction.wrap()
            def persist:
                %*\ldots*)
        \end{pycode}
        \begin{rbcode}[gobble=12]
            def persist
                # ??
            end
        \end{rbcode}
    \end{slide}

    \begin{note}{}
        \paragraph{}
        transaction.wrap is also available as a decorator. A decorator wraps a
        function inside of another one and updates the variable containing the
        inner function with the result. Is there a closer equivalent for
        decorators in Ruby? Please tell me!
    \end{note}

    \begin{slide}[method=direct]{Decorators}
        \begin{pycode}[gobble=12]
            class wrap:
                %*\ldots*)
                def __call__(self, fn):
                    @wraps(fn)
                    def _inner(*args, **kwargs):
                        with self:
                            return fn(*args, **kwargs)
                    return _inner
                %*\ldots*)
        \end{pycode}
    \end{slide}

    \begin{note}{}
        \paragraph{}
        A decorator is a callable that takes a callable as an argument and
        returns another one.
    \end{note}

    \section{Operator Overloading}

    \begin{slide}[method=direct]{Operator Overloading}
        \begin{pycode}[gobble=12]
            Q(%*\ldots*)) & Q(%*\ldots*)) | Q(%*\ldots*))
        \end{pycode}
        \begin{rbcode}[gobble=12]
            Q(%*\ldots*)) & Q(%*\ldots*)) | Q(%*\ldots*))
        \end{rbcode}
    \end{slide}

    \begin{note}{}
        \paragraph{}
        I implemented operator overloading for the Q objects and Querysets for
        constructing queries.
    \end{note}

    \section{Metaclasses}

    \begin{slide}[method=direct]{Metaclasses}
        \begin{pycode}[gobble=12]
            class Foo(object):
                __metaclass__ = Bar
        \end{pycode}
        \begin{rbcode}[gobble=12]
            class Foo
                extend Bar
            end
        \end{rbcode}
    \end{slide}

    \begin{note}{}
        \paragraph{}
        In python classes are objects as every other object. Every class is
        the instance of a metaclass. Most classes use "type" as metaclass.
        \paragraph{}
        The two code samples are not equivalent because I would have used
        another approach implementing Models in Ruby then in python.
    \end{note}

    \section{Bugs \& Shortcomings}

    \begin{slide}{Bugs \& Shortcomings}
        \vspace{\stretch{1}}
        \begin{itemize}
            \item python 3 only
            \item delete not implemented yet
            \item concurrent transactions are not handled correctly
        \end{itemize}
        \vspace{\stretch{1}}
    \end{slide}

    \section{Up next}

    \begin{slide}{Up next \& Ideas}
        \vspace{\stretch{1}}
        \begin{itemize}
            \item python 2 \& 3 support with six
            \item merging of concurrent transactions
            \item more query optimizations
        \end{itemize}
        \vspace{\stretch{1}}
    \end{slide}

    \begin{slide}{}
        \centering
        \vspace{\stretch{1}}
        {\huge Thx!}
        \par
        \vspace{3em}
        {\large \tt \$ pip install git-orm}
        \vspace{\stretch{1}}
    \end{slide}

\end{document}
