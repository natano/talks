Grenzen von Porters Deutsch-Stemmer

Ein Stemmer ist ein Werkzeug, das Wörter auf ihren Stamm reduziert und
Verwendung in der Verbesserung der Suchleistung computergestützter
Textabfragesysteme findet. Es handelt sich dabei nicht um ein explizit
linguistisches Verfahren. Der erste Stemming-Algorithmus wurde 1968 von Julie
Beth Lovins geschrieben. Bis zur Gegenwart entstanden  - insbesondere wegen der
intensivierten Nutzung des Internets - zahlreiche weitere Algorithmen, unter
denen jener von Martin Porter aufgrund seiner Effizienz und Popularität
hervorgehoben werden kann. 2009 adaptierte Porter den ursprünglich für die
englische Sprache konzipierten Algorithmus auch für deutsche Sprache. Da dieser
Stemmer bei der Anwendung einige Schwachstellen aufweist, ergab sich die
Aufgabe, die Fehlerokkurrenzen in Kategorien zu fassen und zu beschreiben. 
