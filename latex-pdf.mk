.PHONY: clean clean-pdf clean-all
.SUFFIXES: .tex .dvi .pdf

LATEX ?= latex
DVIPDF ?= dvipdf


.tex.dvi:
	${LATEX} $<
	${LATEX} $<

.dvi.pdf:
	${DVIPDF} $< $@

clean:
	rm -f *.aux *.bm *.dvi *.log *.toc

clean-pdf:
	rm -f *.pdf

clean-all: clean clean-pdf
